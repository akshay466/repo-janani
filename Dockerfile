FROM mcr.microsoft.com/windows/servercore/iis
RUN mkdir "C:\Project"
WORKDIR "C:\Project"
COPY ProductMicroservice.zip .
RUN powershell -command expand-archive .\ProductMicroservice.zip .
CMD ["ping", "-t", "localhost"]